import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { CreatePostComponent } from './create-post/create-post.component';
import { SignFormComponent } from './sign-form/sign-form.component';
import { UserMinimalInterface } from './user/user.interface';
import { AuthService } from './shared/auth.service';

import { LoaderService } from "./shared/loader.service";
import { PostService } from './post/post.service';
import { DpPostInterface } from './post/post.interface';
import { Config } from './config';

declare var gapi: any;
declare var FB: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ AuthService, LoaderService ]
})

export class AppComponent implements OnInit {
  title = 'app';
  sections: any[] = ['gg'];
  loggedIn;
  navToggle: boolean = false;
  user: UserMinimalInterface;
  // Used for telling path to view for sidebar
  path: string = '/';
  featured: DpPostInterface[];

  constructor(
    public modalService: NgbModal,
    private post: PostService, 
    private auth: AuthService, 
    private loader: LoaderService, 
    public activatedRoute: ActivatedRoute,
    public route: Router) {
    this.loggedIn = this.auth.isLoggedIn();
    if(this.loggedIn){
      this.user = this.auth.getUser();
    }
    this.sections = Config.categories;
    this.sections.sort();
  }

  ngOnInit() { 
    this.route.events.subscribe((event)=>{
      if(event instanceof NavigationEnd){
        this.path = (event as any).url;
      }
    })
    this.getFeatured();
  }

  toggleNav(){
    this.navToggle = !this.navToggle;
  }
  
  openCreatepost($event) {
    $event.preventDefault();
    this.modalService.open(CreatePostComponent);
  }

  openSignInComponent($event) {
    $event.preventDefault();
    this.modalService.open(SignFormComponent);
  }

  openRegisterComponent($event) {
    $event.preventDefault();
    let ref = this.modalService.open(SignFormComponent);
    ref.componentInstance.activeTab = "register";
  }

  onSearch(){
    this.toggleNav();
  }

  getFeatured(){
    this.post.getFeatured().subscribe((posts: DpPostInterface[]) =>{
      this.featured = posts;
    })
  }

  logout(){
    let account_type = this.auth.getUser().account_type;
    this.loader.show("Logging out...")
    .then(()=>{
      this.auth.logout();
      this.loggedIn = this.auth.isLoggedIn();
      window.location.href = '/';
    })
  }

  logoutFB(){
    if(typeof FB != 'undefined'){
      FB.getLoginStatus(res=>{
        if(res.authResponse){
          console.log("Logging out")
          return FB.logout(res=>{
            this.auth.logout();
            this.loggedIn = this.auth.isLoggedIn();
            window.location.reload();
            return true;
          });
        }
      }); 
    }else{
      this.loader.hide("Try again...");
    }
  }
}
