import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

// Components
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { CreatePostModule } from './create-post/create-post.module';
import { CreatePostComponent } from './create-post/create-post.component';
import { SignFormComponent } from './sign-form/sign-form.component';
import { ReportComponent } from './report/report.component';
import { ReportCreateComponent } from './report/report-create/report-create.component';

// Services
import { LoaderComponent } from "./shared/loader.component";
import { LoaderService } from "./shared/loader.service";

// Routes 
import { AppRoutes } from "./app.routes";

import { UserComponent } from './user/user.component';
import { FeaturedComponent } from './featured/featured.component';
import { ReviewComponent } from './review/review.component';
import { UsersComponent } from './users/users.component';

import { SharedComponentModule } from './shared/shared-components.module';
import { PostModule } from './post/post.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    SignFormComponent,
    LoaderComponent,
    UserComponent,
    ReportComponent,
    ReportCreateComponent,
    FeaturedComponent,
    ReviewComponent,
    UsersComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    PostModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedComponentModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 10000,
      closeButton: true,
      positionClass: 'toast-bottom-left',
      preventDuplicates: true,
    }),
    RouterModule.forRoot(AppRoutes),
    CreatePostModule,
    HttpClientModule
  ],
  exports: [RouterModule, PostModule],
  providers: [NgbActiveModal, LoaderService, { provide: 'Window',  useValue: window }],
  bootstrap: [AppComponent, SearchComponent],
  entryComponents: [PageNotFoundComponent, CreatePostComponent, SignFormComponent, LoaderComponent, ReportCreateComponent, FeaturedComponent]
})

export class AppModule { }
