import { Routes } from "@angular/router";

import { UserComponent } from "./user/user.component";
import { ReportComponent } from './report/report.component';
import { ReviewComponent } from './review/review.component';
import { UsersComponent } from "./users/users.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const AppRoutes: Routes = [
  { path:'', loadChildren:'./post/post.module#PostModule' },
  // User Routes
  { path:'u/:id', pathMatch: 'full', component: UserComponent },
  { path:'settings', loadChildren:'./settings/settings.module#SettingsModule' },
  // Admin Routes
  { path: 'reports/posts', pathMatch: 'full', component: ReportComponent},
  { path: 'reviews/posts', pathMatch: 'full', component: ReviewComponent},
  { path: 'users', pathMatch: 'full', component: UsersComponent},
  // Reactions
  { path: 'reactions', loadChildren:'./reactions/reactions.module#ReactionsModule' },
  { path: '**', component: PageNotFoundComponent }
]
