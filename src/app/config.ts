const config = {
  host :  "http://dopachan:80/api",
  ACCOUNT_TYPE_LOCAL: 0,
  ACCOUNT_TYPE_FB: 1,
  ACCOUNT_TYPE_GOOG: 2,
  categories: [
    'Anime', 'Pets', 'Discovery', 'Photography', 'Cars', 'Comics', 'Travel', 'Gaming', 'Food', 'Funny', 'Craft', 'Creative',
    'Models', 'Girly', 'Movies', 'Music', 'Politics', 'Savage', 'Sports', 'Sci-Tech', 'WTF', 'School', 'Wallpapers', 'Videos'
  ]
};

export const Config = config;