import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

import { ReportCreateComponent } from '../../report/report-create/report-create.component';

import { CommentService } from './comments.service';
import { UserMinimalInterface } from '../../user/user.interface';
import { AuthService } from '../../shared/auth.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as Autolinker from 'autolinker';

@Component({
  selector: 'dp-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() id: number;
  @Input() postId: string;
  @Input() user: UserMinimalInterface; 
  @Input() displayName: string;
  @Input() comment: string;
  @Input() likes: number = 0;
  @Input() dislikes: number = 0;
  @Input() liked: boolean = false;
  @Input() disliked: boolean = false;
  @Input() replies: number = 0;
  @ViewChild('moreContent') popOver; 
  @ViewChild('overlayer') overlayer;
  likesCount: number = 0;
  me: UserMinimalInterface;
  action: number = 0;
  private compensate :number = 0;
  
  constructor(private elRef: ElementRef, private _comment:CommentService, private _auth: AuthService, private _toats: ToastrService, private _modal: NgbModal) { }

  ngOnInit() {
    if(this._auth.isLoggedIn()){
      this.me = this._auth.getUser();
    }
    if(this.liked){
      this.action = 1;
      this.compensate = -1;
    }else if(this.disliked){
      this.action = -1;
      this.compensate = +1;
    }else{
      this.action = 0;
      this.compensate = 0;
    }
    this.updateCount();
    this.comment = Autolinker.link(this.comment, {
      'truncate': 24
    });
  }

  like($event){
    if(this.action == 0 || this.action == -1){
      let prevVal = this.action;
      this.action = 1; 
      this._comment.like(this.id).subscribe((res: any)=>{
        if(!res.success){
          this.action = prevVal;
          this._toats.error("Couldn't like comment", 'Error');
        }
      }, err=>{
        this.action = prevVal;
        this._toats.error("Couldn't like comment", 'Error');
      });
    }else{
      let prevVal = this.action;
      this.action = 0;
      this._comment.removeAction(this.id).subscribe((res: any)=>{
        if(!res.success){
          this.action = prevVal;
          this._toats.error("Couldn't like comment", 'Error');
        }
      }, err=>{
        this.action = prevVal;
        this._toats.error("Couldn't like comment", 'Error');
      });
    }
    this.updateCount();
  }

  dislike($event){
    if(this.action == 0 || this.action == 1){
      let prevVal = this.action;
      this.action = -1;
      this._comment.dislike(this.id).subscribe((res: any)=>{
        if(!res.success){
          this.action = prevVal;
          this._toats.error("Couldn't like comment", 'Error');
        }
      }, err=>{
        this.action = prevVal;
        this._toats.error("Couldn't like comment", 'Error');
      });
    }else{
      let prevVal = this.action;
      this.action = 0;
      this._comment.removeAction(this.id).subscribe((res: any)=>{
        if(!res.success){
          this.action = prevVal;
          this._toats.error("Couldn't like comment", 'Error');
        }
      }, err=>{
        this.action = prevVal;
        this._toats.error("Couldn't like comment", 'Error');
      });
    }
    this.updateCount();
  }

  updateCount(){
    this.likesCount = this.likes + (-this.dislikes) + (this.action + this.compensate);
  }

  delete(){
    // Hide comment
    this._toats.info("Deleting comment...");
    this.overlayer.nativeElement.style.display = 'inline-block';
    this._comment.delete(this.id).subscribe(res=>{
      if((res as any).success){
        this.elRef.nativeElement.style.display = 'none';
        this._toats.success("Comment deleted!");
      }else{
        this._toats.error("Comment not deleted!", "Something went wrong!");
        this.overlayer.nativeElement.style.display = 'none';
      }
    },err=>{
      this._toats.error("Comment not deleted!", "Something went wrong!");
      this.overlayer.nativeElement.style.display = 'none';
    })
  }

  report(){
    this.popOver.hide();
    let ref = this._modal.open(ReportCreateComponent)
    ref.componentInstance.content = {id: this.id, posterId: this.user.id, type: 'comment'};
    ref.result.then(result=>{
      if(result == 'remove'){
        this.elRef.nativeElement.style.display = 'none';
      }
    })
  }
}
