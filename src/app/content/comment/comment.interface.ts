export interface DpComment{
  id?: string,
  user: object,
  postId: string,
  displayName: string,
  comment: string,
  replyId?: string,
  votes?: any[]
}