import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from  "@angular/common/http";
import { retry } from "rxjs/operators"

import { Config } from "../../config";
import { AuthService } from "../../shared/auth.service";

@Injectable()
export class CommentService{

  constructor(private _auth: AuthService, private _http: HttpClient){ }

  create(postId: string, comment: string, replyId?: string){ 
    let token = "";
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    return this._http.post( Config.host + "/comments/", {
      post_id: postId, 
      comment: comment
     },
    {'headers': new HttpHeaders({'Authorization': 'Bearer ' + token})}
    )
  }

  delete(id){ 
    let token = "";
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }

    return this._http.delete(Config.host + "/comments/" + id,
      {'headers': new HttpHeaders({'Authorization': 'Bearer ' + token})}
    ).pipe(
      retry(3)
    );
   }

  like(id){ 
    let token = "";
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();  
    }

    return this._http.post(Config.host + '/comments/like', {id: id}, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    }).pipe(
      retry(3)
    );
  }
  dislike(id){
    let token = "";
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();  
    }

    return this._http.post(Config.host + '/comments/dislike', {id: id}, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    }).pipe(
      retry(3)
    );
   }
  removeAction(id){
    let token = "";
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();  
    }

    return this._http.post(Config.host + '/comments/removelike', {id: id}, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    }).pipe(
      retry(3)
    );
  }
}