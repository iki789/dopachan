import { Component, ElementRef, HostBinding, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { PostService } from "../post/post.service";
import { DpPostInterface } from "../post/post.interface";
import { AuthService } from "../shared/auth.service";
import { Reaction } from '../reactions/reactions';
import { ReactionService } from 'app/reactions/reactions.service';

@Component({
  templateUrl: 'content.component.html',
  styleUrls: [ 'content.component.scss' ],
  providers: [ PostService ]
})


export class ContentComponent implements OnInit {
  posts: DpPostInterface[] = [];
  public showMorePosts: boolean = true;
  section: string = "";
  reactions: Reaction[];
  @HostBinding('class') 'classes' = 'col-md-6'; 
  @ViewChild("loadPostElement") loadPostElement: ElementRef;

  constructor( private _auth: AuthService, private route: ActivatedRoute,  private postService: PostService, private reactionService: ReactionService) {
    if(this._auth.isLoggedIn()){
      const _user = this._auth.getUser();
    }
   }

  ngOnInit() {
    // Reset LoadMorePost to get more posts
    this.showMorePosts = true;
    this.route.url.subscribe(queryStings=>{
      // Get second param
      if(queryStings[1]){
        this.section = queryStings[1].path.toString().split('/').pop();
      }
      this.getPosts(true, this.section);
    });
    this.reactionService.getAll().subscribe((r: Reaction[])=>{
      this.reactions = r;
    })
  }

  loadMorePosts($event){
    if(this.posts.length < 1) return false;
    if($event.visible && this.loadMorePosts){
      this.postService.getFeeds(this.section, {offset: this.posts.length, limit: 10, sort: -1}).subscribe((res: DpPostInterface[])=>{
        this.posts = this.posts.concat(res);
        if(res.length < 1){
          this.showMorePosts = false;
        }
      })
    }
  }

  getPosts($event, section){
    this.postService.getFeeds(section, {offset: this.posts.length, limit: 10, sort: -1}).subscribe((res: DpPostInterface[])=>{
      this.posts = res;
      console.log(this.posts)
      if(res.length < 1){
        this.showMorePosts = false;
      }
    })
  } 
}
