import { Component, ElementRef, EventEmitter , Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';

import { AuthService } from "../../shared/auth.service";
import { UserMinimalInterface } from "../../user/user.interface"
import { DpComment } from "../comment/comment.interface";

@Component({
  selector: 'dp-new-comment',
  template: `
    <div class="avatar mr-1" [ngStyle]="{backgroundImage: 'url('+user.avatar+')'}"></div>
    <div class="form-group text-area-wrapper">
      <div *ngIf="textEmpty" class="empty"></div>
      <div *ngIf="disabled" class="disabled"></div>
      <div 
        #el
        (keyup)="onKeyup($event)" 
        (keydown)="onKeydown($event)" 
        class="text-area form-control" 
        contentEditable></div>
    </div>
  `,
  styles: [`
    :host{
      display: flex;
    }
    :host img{ 
      align-self: center; 
      border-radius: 2px;
      margin-right: 0.5rem;
    }
    :host .form-group{
      margin-bottom: 0px;
      flex-grow: 1;
    }
    .avatar{
      width: 34px;
      height: 34px;
      background-size: cover;
      background-position: center;
      border-radius: 2px;
    }
    .text-area-wrapper{
      width: 90%;
      position: relative;
    }
    .text-area{
      max-width: 100%;
      overflow-wrap: break-word;
      height: 100%;
    }
    .empty::before{
      content: "Write a comment...";
      position: absolute;
      top: 5px;
      left: 15px;
      color: #d3d3d3;
      pointer-events: none;
    }
    .disabled{
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0px;
      left: 0px;
      background-color: rgba(211,211,211,0.4);
    }
  `], 
})
export class NewCommentComponent implements OnInit, OnChanges {

  @ViewChild('el') el: ElementRef;
  @Input() user: UserMinimalInterface;
  @Input() postId: string;
  @Input() replyId: string = "";
  @Input() disabled: boolean = false;
  @Input() submitState: boolean = false; 
  @Output() onSubmit = new EventEmitter();
  textEmpty: boolean = true;

  constructor(private _auth: AuthService) { }

  ngOnInit() { 
    if(this._auth.isLoggedIn()){
      this.user = this._auth.getUser();
    }else{
      this.user = {
        id:"",
        avatar: "./assets/images/avatar.png",
        display_name: "",
        group: 0
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.submitState){
      if(changes.submitState.currentValue == true){
        this.clearText();
      }
    }
  }

  onKeydown($event){
    if(this.disabled) return false;
    if($event.keyCode == 13 && !$event.shiftKey){
      if(!this._auth.isLoggedIn()){
        this.onSubmit.emit();
        return false;
      }
      // Submit
      let data: DpComment = {
        user: {
          "id": this.user.id,
          "display_name": this.user.display_name,
          "avatar": this.user.avatar
        },
        postId: this.postId, 
        displayName: this.user.display_name,
        comment: $event.target.textContent, 
      };
      if(this.replyId.length != 0){
        data.replyId = this.replyId;
      }
      this.onSubmit.emit(data);
      // Prevent any further actions
      return false;
    }
    if($event.target.textContent == ""){
      this.textEmpty = true;
    }else{
      this.textEmpty = false;
    }
  }

  onKeyup($event){
    if($event.target.textContent == ""){
      this.textEmpty = true;
    }else{
      this.textEmpty = false;
    }
  }

  private clearText(){
    this.el.nativeElement.textContent = "";
  }
}
