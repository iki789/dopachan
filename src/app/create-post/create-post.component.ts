import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import { Config } from '../config';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

import { Reaction } from '../reactions/reactions';
import { LoaderService } from '../shared/loader.service';
import { AuthService } from '../shared/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})

export class CreatePostComponent implements OnInit, AfterViewInit {

  @ViewChild('wrapper') wrapper: ElementRef;
  activeEl;
  page = 0;
  obj: MvPostForm = {
    token: '',
    file: { 
      path: "",
      next: ""
    },
    caption: "",
    reacts: [],
    tags:[],
    nsfw: false
  }

  constructor(
    public modalRef: NgbActiveModal,
    public _toast: ToastrService,
    private _auth: AuthService, 
    private http: HttpClient, 
    private el: ElementRef, 
    private renderer: Renderer2,
    private loader: LoaderService
  ){ }
  ngOnInit(){ 
    
  }
  ngAfterViewInit(){ 
    this.activeEl = this.el.nativeElement.querySelector('.active');
  }

  close() {
    this.modalRef.close();
  }

  private next() {
    if(this.activeEl.nextElementSibling != null){
      this.page++;

      this.wrapper.nativeElement.style.transform = "translate(calc("+this.page+"*-33%))";
      this.activeEl = this.activeEl.nextElementSibling;
      this.activeEl.classList.add("active");
      this.renderer.listen(this.wrapper.nativeElement ,'transitionend', ()=>{
        if(this.activeEl.previousElementSibling){
          this.activeEl.previousElementSibling.classList.remove("active");
        }
      });
    }
  }

  private prev() {
    if(this.activeEl.previousElementSibling != null){
      this.page--;

      this.wrapper.nativeElement.style.transform = "translate(calc("+this.page+"*-33%))";
      this.activeEl = this.activeEl.previousElementSibling;
      this.activeEl.classList.add("active");
      this.renderer.listen(this.wrapper.nativeElement ,'transitionend', ()=>{
        if(this.activeEl.nextElementSibling){
          this.activeEl.nextElementSibling.classList.remove("active");
        }
      });
    }
  }

  setImageLocation(fileLocation) {
    this.obj.file = fileLocation;
    if(fileLocation.next && fileLocation.path != null){
      this.next();
    }
  }

  getData($event) {
    let reactions = []; 
    let token = this._auth.getJwtToken();
    // $event.reactions.forEach((x, i) => {
    //   reactions.push(x);
    // });
    this.obj.reacts = $event.reactions;
    this.obj.tags = $event.tags;
    this.obj.caption = $event.caption;
    this.obj.nsfw = $event.nsfw;
    this.obj.token = token; 
    // submit form

    let req = new HttpRequest('POST', Config.host + "/post",  this.obj,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    });
    this.http.request(req)
      .subscribe(res=>{
        if(res.type == 0){
          // Request in progress
          this.loader.show("Creating post...")
        }
        if(res.type == 4){
          // Response received
          let success = (res as any).body.success;
          if(success){
            this._toast.success('Post created!');
            this.loader.hide()
            this.modalRef.close();
          }else{
            this._toast.error('Something horribaly went wrong!', 'Try agian');
            this.modalRef.close();
            this.loader.hide();
          }
        }
        // console.log(x);
    },err=>{
      this._toast.error('Something horribaly went wrong!', 'Try agian');
      this.modalRef.close();
      this.loader.hide();
    })
  }

}

export interface MvPostForm {
  token: string,
  file: {
    path: string,
    next: any
  },
  caption: string,
  reacts: any[],
  tags: any[],
  nsfw: boolean
}