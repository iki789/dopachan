import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { TagInputModule } from 'ngx-chips';
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';

import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { CreatePostComponent } from './create-post.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { SelectReactionsComponent } from './select-reactions/select-reactions.component';
import { TagsComponent } from './tags/tags.component';
import { AuthService } from "../shared/auth.service"; 
import { SharedComponentModule } from 'app/shared/shared-components.module';

TagInputModule.withDefaults({
  tagInput: {
    placeholder: '',
    // add here other default values for tag-input,
    secondaryPlaceholder: "e.g. funny, wait-for-it",
    modelAsStrings: true
  },
})

@NgModule({
  imports:[
    FormsModule, 
    BrowserModule, 
    BrowserAnimationsModule, 
    TagInputModule,
    NgbProgressbarModule,
    VgCoreModule,
    VgControlsModule,
    SharedComponentModule
  ],
  exports:[CreatePostComponent, RouterModule],
  declarations:[CreatePostComponent, UploadFileComponent, SelectReactionsComponent, TagsComponent],
  providers:[ AuthService ],
  entryComponents:[UploadFileComponent]
})

export class CreatePostModule{ }