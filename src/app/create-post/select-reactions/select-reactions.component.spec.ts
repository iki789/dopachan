import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectReactionsComponent } from './select-reactions.component';

describe('SelectReactionsComponent', () => {
  let component: SelectReactionsComponent;
  let fixture: ComponentFixture<SelectReactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectReactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectReactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
