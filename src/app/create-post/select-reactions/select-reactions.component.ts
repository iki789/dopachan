import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';

import { Reaction } from "../../reactions/reactions";
import { ReactionService } from '../../reactions/reactions.service';
import { Config } from 'app/config';

@Component({
  selector: 'create-post-select-reactions',
  templateUrl: './select-reactions.component.html',
  styleUrls: ['./select-reactions.component.scss']
})

export class SelectReactionsComponent implements OnInit {

  @ViewChild('caption') caption : ElementRef;
  @ViewChild('checkboxNsfw') checkboxNsfw : ElementRef; 

  @Input() file : string ;
  @Output() data = new EventEmitter();
  activatedSelection = false;
  numberOfSlots = new Array(5);
  selectedIndex: number;
  selectedReactions: Reaction[];
  reactions: Reaction[];
  tags: any[] = [];
  nsfw: boolean;
  tagsError: string;
  category: string = "";
  categoryError: string;
  categoryList: string[];
  minTags : number = 2;
  isVid : boolean = false;

  constructor(private reactionService: ReactionService) {
    this.categoryList = Config.categories;
  }

  ngOnInit() { 
    this.reactionService.getAll().subscribe((reacts: Reaction[])=>{
      this.reactions = reacts;
      this.selectedReactions = this.reactionService.filterDefault(this.reactions);
    });
  }

  openReactions(index){
    this.selectedIndex = index;
    this.activatedSelection = true;
  }
  
  ngOnChanges(change : SimpleChanges){
    let changes : any = change;
    if(changes.file.currentValue != ""){  
      let ext = changes.file.currentValue.split(".").pop().toLowerCase();
      this.isVid = ["mp4"].indexOf(ext) != -1 ? true : false ;
    }
  }
  reactionSelected($event){
    this.activatedSelection = false;
    if($event){
      this.selectedReactions[$event.index] = $event.reaction;
    }
  }

  getTags($event){
    this.tags = $event;
  }

  submit(){
    if((this.category.trim().length) < 1 && this.categoryList.indexOf(this.category) < 0){
      this.categoryError = `Select a valid category`;
    }else if(this.tags.length < this.minTags){
      this.tagsError = `Atleast ${this.minTags} tags required`;
    }else{
      // push category to tags list
      this.tags.push(this.category.toLowerCase().replace(" ", "-"));
      this.nsfw = this.checkboxNsfw.nativeElement.checked;
      this.data.emit({caption: this.caption.nativeElement.value ,reactions: this.selectedReactions, tags: this.tags, nsfw: this.nsfw, next: true});
    }
  }

}