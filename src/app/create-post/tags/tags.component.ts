import { Component, EventEmitter, OnInit, Output } from '@angular/core';
@Component({
  selector: 'create-post-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
  items : any[] = ['funny', 'aww'];
  @Output() tags = new EventEmitter();
  
  constructor() { }
  
  ngOnInit(){ }
  
  onSelected($event){
    let tagName = $event;
    // Remove clicked item
    this.items = this.items.filter(item => { return item != tagName });
    this.tags.emit(this.items);
  }

  onAdd($event){ this.tags.emit(this.items); }

  /**
  * Suggestions Implementions
  * 
  tags = ["anime", "fail", "AOT", "Jasmine", "Erza", "Games-of-Thrones" ];
  suggestions = ["anime", "fais", "AOT", "Jasmine", "Erza"]
  addItem($event){
    let value = $event.target.textContent;
    // Remove from suggestions
    this.suggestions = this.suggestions.filter(val=>{ return val != value });
    this.items.push(value);
  }
  **/

}
