import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Config } from '../../config';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'create-post-upload',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
})

export class UploadFileComponent implements OnInit {

  @ViewChild("dropzoneInput") dropzoneInput: ElementRef;
  @ViewChild("urlInput") urlInput: ElementRef;
  @ViewChild("previewImage") previewImage: ElementRef;

  @Output() fileLocation = new EventEmitter();

  activatedDropzone: boolean = false;
  acceptedFiles: any[] = ["png", "jpg", "jpeg", "gif", "mp4"];
  sizeLimit = 1024*1024*1024;
  error: string;
  uploadInProgress: boolean = false;
  uploadProgress: number = 0;

  constructor(public activeModal: NgbActiveModal, private http: HttpClient, private _auth: AuthService) {}
  ngOnInit(){ }
  ngAfterViewInit(){ }

  clickedDropzone($event){
    this.dropzoneInput.nativeElement.click(); 
  }
  
  droppedFile($event){
    $event.preventDefault();
    let fileList = this.dropzoneInput.nativeElement.files = $event.dataTransfer.files;
    let file = fileList[0];
    this.uploadFile(file);
  }

  private uploadFileToServer(file){
    
    let formData = new FormData();
    formData.append('file', file);
    let req = new HttpRequest('POST', Config.host + "/post/upload", formData, { responseType: "json", reportProgress: true, headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    });

    this.http.request(req)
      .subscribe(e=>{
        if(e.type === HttpEventType.UploadProgress){
          const percentDone = Math.round(100 * e.loaded / e.total);
          this.uploadProgress = percentDone;
        }
        if(e.type == HttpEventType.Response){
          let ShouldGoNext = false;
          let body = e.body as any;
          
          if(e.body != null){
            ShouldGoNext = true;
            if((e.body as any).success != 1){
              ShouldGoNext = false;
              this.error = "Oops! Something went wrong!";
            }
          }

          this.fileLocation.emit({"path": (e.body as any).path, "next": ShouldGoNext})
        }
      })
  }
  
  uploadFile(file){
    if(this.uploadInProgress)return false;
    if(this.verifyDropzoneFile()){
      let previewEl = this.previewImage.nativeElement;
      this.uploadInProgress = true;
      // Read file into preview
      let reader = new FileReader();
      reader.addEventListener('load', ()=>{
        this.previewImage.nativeElement.src = reader.result;
      })
      reader.readAsDataURL(file);
      this.uploadFileToServer(file);
    }
  }

  // when file is selected instead of dragged
  clickedInput($event){
    let file = $event.target.files[0];
    this.uploadFile(file);
  }
  
  onDragover($event){
    $event.preventDefault();
    this.activatedDropzone = true;
  }

  dragEnter($event){
    $event.preventDefault();
    this.activatedDropzone = true;
  }
  
  dragLeave($event){
    $event.preventDefault();
    this.activatedDropzone = false;
  }

  mouseEnterDropzone(){
    this.activatedDropzone = true;
  }

  mouseLeaveDropzone(){
    this.activatedDropzone = false;
  }

  private verifyDropzoneFile(){
    let isValid = false;

    let file: File = this.dropzoneInput.nativeElement.files[0];
    let ext = file.name.split('.').pop().toLowerCase();
    let size = file.size;

    if(this.acceptedFiles.indexOf(ext) == -1){
      console.log(ext)
      this.error = "File type not supported";
      isValid = false;
    }else if(size > this.sizeLimit){
      this.error = "File too large(Max size 1Gb)";
      isValid = false;
    }else{
      this.error = ""
      isValid = true;
    }
    return isValid;
  }

  verifyUrl(){
    console.log(this.urlInput.nativeElement.value);
  }

}