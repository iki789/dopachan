import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DpPostInterface } from '../post/post.interface';
import { PostService } from '../post/post.service';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss']
})
export class FeaturedComponent implements OnInit {

  post: DpPostInterface;
  constructor(private _modalRef: NgbActiveModal, private _post: PostService) { }

  ngOnInit() {
    console.log(this.post);
  }

  close(){
    this._modalRef.close();
  }

}
