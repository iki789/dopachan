import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentComponent } from '../content/content.component';
import { PostViewComponent } from './post-view/post-view.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: ContentComponent },
  { path: 'p/:section', pathMatch: 'full', component: ContentComponent },
  { path: 'post/:id', pathMatch: 'full', component: PostViewComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  entryComponents:[]
})

export class PostRoutingModule { }