import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Routes } from '@angular/router';

import { AuthService } from '../../shared/auth.service';
import { DpPostInterface } from '../post.interface';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {
  @HostBinding('class') 'classes' = 'col-md-6';
  post: DpPostInterface;
  loading = true;

  constructor(
    public _auth: AuthService,
    private _post: PostService,
    private _route: ActivatedRoute
  ) { }
  ngOnInit() {
    this._route.url.subscribe((routes: Routes)=>{
      if(this.post){
        if(routes[1].path){
          if(this.post.id != routes[1].path){
            this.loading = true;
            this._post.getPost(routes[1].path).subscribe(post=>{
              this.post = post[0];
              this.loading = false;
            })
          }
        }
      }else{
        if(routes[1].path){
          this.loading = true;
          this._post.getPost(routes[1].path).subscribe(post=>{
            this.post = post[0];
            this.loading = false;
          }) 
        }
      }
    });
  }

}
