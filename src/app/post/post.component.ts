import { Component, EventEmitter, HostBinding, Input, OnInit, ViewChild, ElementRef, Output } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap" 
import { VgAPI } from 'videogular2/core';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from "../shared/auth.service";
import { PostService } from './post.service';
import { SignFormComponent } from "../sign-form/sign-form.component";
import { CommentService } from "../content/comment/comments.service";
import { ReportCreateComponent } from '../report/report-create/report-create.component';
import { UserMinimalInterface } from '../user/user.interface';
import { DpPostInterface } from './post.interface';
import { UserService } from '../user/user.service';
import { Reaction } from '../reactions/reactions';
import { ReactionService } from 'app/reactions/reactions.service';

@Component({
  selector: 'dp-post',
  templateUrl: './post.component.html',
  styleUrls: [ './post.component.scss' ],
  providers: [ UserService, CommentService ]
})
export class PostComponent implements OnInit {

  @HostBinding("class") class = "mb-3";

  @Input() post: DpPostInterface;
  @Input() reactions: Reaction[];
  @Input() showComments: boolean = true ;
  @Input() showUser: boolean = false ;
  @Input() showTags: boolean = false ;
  @Input() showReactions: boolean = true ;
  @Input() editMode: boolean = false ;
  @Input() link: boolean = false ;
  @Input() defaultReactions: Reaction[];
  @Input() showNsfwFlag: boolean = false ;
  @Input() limitHeight: boolean = false ; 
  @Output() onUpdate = new EventEmitter ;
  @ViewChild("linkEl") linkEl: ElementRef;
  @ViewChild("playBtn") playBtn: ElementRef;
  @ViewChild("reactionElements") reactionElements : ElementRef; 
  user: UserMinimalInterface = {
    avatar: '',
    display_name: '',
    id: '',
    group: 1
  };
  public reactCount:number = 0;
  private vgApi;
  private video;
  public fullScreenToggle: boolean = false ;
  public muted: boolean = true;
  public showReactsCount = false;
  public submittingComment: boolean = false ;
  public submittingCommentState: boolean = false;
  public showReactionList: boolean = false ;

  constructor(
    private _auth: AuthService, 
    private _toastr: ToastrService,  
    private modalService: NgbModal, 
    private commentService: CommentService, 
    private _post: PostService,
    private _reaction: ReactionService,
    private _user: UserService,
    public el: ElementRef) { }

  ngOnInit() {
    
    // User
    if(!this._auth.isLoggedIn()){
      this.user.group = 0;
    }else{
      this.user = this._auth.getUser();
    }
    if(!this.post.comments){
      this.post.comments = [];
    }

    if(!this.reactions){
      this._reaction.getAll().subscribe((r: Reaction[])=>{
        this.reactions = r;
        this.sortReactsArray();
      });
    }else{
      this.reactions = JSON.parse(JSON.stringify(this.reactions));
      this.sortReactsArray();
    }
    // Show reacts count if any 1 reaction is liked
    for(let r of this.post.reacts){
      this.reactCount = this.reactCount + r.count;
      if(this.showReactsCount){ continue; }
      if(r.liked){ this.showReactsCount = true; break; }
    }
  }

  onPlayerReady(api: VgAPI) {
    this.vgApi = api;
    if(this.post.isVid){
      this.video = this.vgApi.getDefaultMedia().vgMedia;
      this.vgApi.fsAPI.onChangeFullscreen.subscribe(isFullscreen=>{
        this.fullScreenToggle = isFullscreen;
      })
    }
  }

  playVideo($event="", id="") {
    (this.playBtn as any).elem.style.display = 'none';
  }

  reactToEmo($event) {
    $event.stopPropagation();
    if(typeof $event.stopPropagation == 'function')$event.stopPropagation();
    if(!this._auth.isLoggedIn()){
      this.modalService.open(SignFormComponent);
      return false;
    }
    
    let reactions: HTMLCollection = this.reactionElements.nativeElement.children;
    let reactionsArray = Array.from(reactions);
    let el: HTMLElement = $event.target;

    for(let index in  reactionsArray){
      // Remove all selections
      let child = reactions[index].children[0];
      if(child.id != el.id){
        child.classList.remove("selected");
      }
    }    
    
    // Select Reaction 
    if(el.classList.contains('selected')){
      el.classList.remove("selected");
      // Update object
      this.updateReactsArray(el.id);
      this.showReactsCount = false;
      // Update in database
      this._post.react(this.post.id, (el as any).id, true).subscribe((x: any)=>{
        if(!x.success){
          el.classList.add('selected');
          this._toastr.error("Couldn't react to post!", "Error");
        }
      },err=>{
        el.classList.add('selected');
        this._toastr.error("Couldn't react to post!", "Error");
      });
    }else{
      // console.log(el)
      el.classList.add("selected");
      // Update object
      this.updateReactsArray(el.id);
      this.showReactsCount = true;
      // Update in database
      this._post.react(this.post.id, (el as any).id).subscribe((x:any)=>{
        if(!x.success){
          el.classList.remove('selected');
          this._toastr.error("Couldn't react to post!", "Error");
        }
      },err=>{
        el.classList.remove('selected');
        this._toastr.error("Couldn't react to post!", "Error");
      });
    }

  }

  onSelectCustomReaction(data){
    if(!data) return this.showReactionList = !this.showReactionList;
    if(!this._auth.isLoggedIn()){
      this.modalService.open(SignFormComponent);
      return false;
    }
    let r = data.reaction;
    r.liked = true;
    r.count = 1;
    this.post.reacts[4] = r;
    this.showReactsCount = true;
    let reactions: HTMLCollection = this.reactionElements.nativeElement.children;
    let el = reactions[4];

    // Remove `selected` class from all elements
    for(let index in  Array.from(reactions)){
      // Remove all selections
      let child = reactions[index].children[0];
      if(child.id != el.id){
        child.classList.remove("selected");
      }
    }
    
    // Add `selected` class to current element
    el.classList.add('selected');
    this._post.react(this.post.id, r.id).subscribe((react: any)=>{
      if(!react.success){
        el.classList.remove('selected');
        this._toastr.error("Couldn't react to post!", "Error");
      }
    },err=>{
      el.classList.remove('selected');
      this._toastr.error("Couldn't react to post!", "Error");
    });
    return this.showReactionList = !this.showReactionList
  }

  removeTag($event){
    let tagName = $event;
    // Remove clicked item
    this.post.tags = this.post.tags.filter(item => { return item != tagName });
    this.onUpdate.emit(this.post)
  }

  addedTag($event){
    this.onUpdate.emit(this.post)
  }

  private updateReactsArray(selectedId){
    for(let i in this.post.reacts){
      if(this.post.reacts[i].liked){
        this.post.reacts[i].count =  this.post.reacts[i].count -1;
        this.post.reacts[i].liked = false;
        continue;
      }
      if(this.post.reacts[i].id == selectedId){
        this.post.reacts[i].count =  this.post.reacts[i].count +1;
        this.post.reacts[i].liked = true;
      }
    }
    // this.sortReactsArray();
  }
  
  private sortReactsArray(){
    let reacts = this.post.reacts;
    let defaultReacted = reacts.filter(r=>{
      return r.default;
    })
    
    this.defaultReactions = this.reactions.filter(r=>{
      return r.default;
    }).map(r=>{
      r.count = 0;
      r.liked = false;
      return r;
    })
    
    let sortedReacts = [];
    reacts.forEach(r=>{
      if(!r.default && sortedReacts.length < 5){
        sortedReacts.push(r); 
      }
    })
    for(let i=0; i < 5; i++){
      if(sortedReacts.length < 5){
        if(defaultReacted[i]){
          sortedReacts.push(defaultReacted[i])
        }else{
          if(defaultReacted[i-1]){
            if(defaultReacted[i-1].id == this.defaultReactions[i].id){
              sortedReacts.push(this.defaultReactions[i-1])
            }else{
              sortedReacts.push(this.defaultReactions[i])
            }
          }else{
            sortedReacts.push(this.defaultReactions[i])
          }
        }
      }
    }

    this.post.reacts = sortedReacts;
    this.post.reacts.sort((a,b)=>{ return b.count-a.count });
  }

  markAsFeatured(){
    this._post.markAsFeatured(this.post.id).subscribe((res: any)=>{
      if(res.success){
        this.post.featured = true;
        this._toastr.success('Marked as featured');
      }else{
        this.post.featured = null;
        this._toastr.success('Failed to make as featured', 'Error');
      }
    });
  }

  removeFromFeatured(){
    this._post.removeFromFeatured(this.post.id).subscribe((res: any)=>{
      if(res.success){
        this.post.featured = null;
        this._toastr.success('Removed from featured');
      }else{
        this.post.featured = true;
        this._toastr.success('Failed to remove from featured', 'Error');
      }
    });
  }

  openSignInComponent() {
    this.modalService.open(SignFormComponent);
  }

  onCommentSubmit(data){
    if(!this._auth.isLoggedIn()){
      this.openSignInComponent();
      return false;
    }
    if(data.comment.trim().length < 1){ return false;}
    // Get token
    const token = this._auth.getJwtToken();
    const user = this._auth.getUser();

    this.submittingComment = true;
    this.commentService.create(data.postId, data.comment).subscribe(x=>{
      this.submittingComment = false;
      // Set state to true to empty comment area
      this.submittingCommentState = true;
      this.post.comments.push({
        id: (x as any).id, 
        user: user,
        displayName: user.display_name, 
        postId:data.postId, 
        comment: data.comment, 
        votes: []
      });  
      setTimeout(x=>{
        // Set it back to false
        this.submittingCommentState = false;
      },500)
    },err=>{
      this.submittingComment = false;
      this.submittingCommentState = false;
      // popuop something went wrong
      this._toastr.error("Could not post comment");
    })
  }

  onInViewportChange($event){
    if($event.visible){
      this.playVideo();
      this.video.volume = 0;
      this.video.play();
    }else{
      this.video.pause();
      this.video.volume = 0;
    }
  }

  onVideoClick(){
    // Just mute/unmute
    this.video.volume = !this.video.volume;
  }

  report(){
    if(!this._auth.isLoggedIn()){
      this.modalService.open(SignFormComponent);
      return false;
    }
    let ref = this.modalService.open(ReportCreateComponent);
    ref.componentInstance.content = {id: this.post.id, posterId: this.post.user_id, type: 'post'};
    ref.result.then(result=>{
      if(result == 'remove'){
        this.el.nativeElement.style.display = 'none';
      }
    })
  }

  share(){
    let postUrl = encodeURIComponent('https://static.dopachan.com/?p='+this.post.id);
    let hashtag = '#'+(this.post.tags ? this.post.tags[0] : 'dopachan');
    hashtag = encodeURIComponent(hashtag)
    let url = `https://www.facebook.com/dialog/share?app_id=183294449062371&display=popup&href=${postUrl}&hashtag=${hashtag}`;
    window.open(url, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
  }

  copyLink(){
    this.linkEl.nativeElement.select();
    document.execCommand('copy');
    this.linkEl.nativeElement.setSelectionRange(0,0);
    this._toastr.info('Link copied to clipboard');
    setTimeout(()=>{
      this._toastr.clear();
    },1500)
  }
  
  addToCollection(){
    if(!this._auth.isLoggedIn()){
      this.modalService.open(SignFormComponent);
      return false;
    }
    this._toastr.success('Post added to your collection');
    this.post.saved = true;
    this._user.addPostToCollection(this.post.id).subscribe((res: any)=>{
      if(!res.success){
        this.post.saved = true;
        this._toastr.error('Error','Post was not added to your collection');
      }
    },err=>{
      this.post.saved = false;
      this._toastr.error('Error','Post was not added to your collection');
    });
  }

  removeFromCollection(){
    if(!this._auth.isLoggedIn()){
      this.modalService.open(SignFormComponent);
      return false;
    }
    this._toastr.success('Post remvoed from your collection');
    this.post.saved = false;
    this._user.removePostFromCollection(this.post.id).subscribe((res: any)=>{
      if(!res.success){
        this.post.saved = true;
        this._toastr.error('Error','Post was not removed from your collection');
      }
    },()=>{
      this.post.saved = true;
      this._toastr.error('Error','Post was not removed from your collection');
    });
  }

  remove(){
    if(confirm("Are you sure you want to delete this?")){
      this.el.nativeElement.style.display = 'none';
      
      this._post.remove(this.post.id, this.post.user_id, 0).subscribe((res: any)=>{
        if(!res.success)this.el.nativeElement.style.display = 'block';
      },err=>{
        this.el.nativeElement.style.display = 'block';
      });
    }
  }

  outputUpdatedPost(){
    this.onUpdate.emit(this.post)
  }

}
