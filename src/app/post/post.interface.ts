import { Reaction } from '../reactions/reactions'
import { DpComment } from '../content/comment/comment.interface';

export interface DpPostInterface{
  id: string,
  caption: string,
  source: string,
  reacts: Reaction[],
  comments: DpComment[],
  isVid?: number,
  tags?: any[], 
  featured?: any,
  nsfw?: boolean,
  user_id?: string,
  thumbnail?: string,
  views?: number,
  saves?:number,
  saved?:boolean
}