import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { PostRoutingModule } from './post-routing.module';

// Video Modules
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';

// Tags
import { TagInputModule } from 'ngx-chips';

import { PostComponent } from './post.component';
import { PostViewComponent } from './post-view/post-view.component';
import { PostService } from './post.service';
import { CommentComponent } from '../content/comment/comment.component';
import { NewCommentComponent } from '../content/new-comment/new-comment.component';
import { CommentService } from '../content/comment/comments.service'
import { ContentComponent } from '../content/content.component';

import { SharedComponentModule } from '../shared/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostRoutingModule,
    VgCoreModule,
    VgControlsModule,
    TagInputModule,
    SharedComponentModule
  ],
  declarations: [
    PostViewComponent,
    PostComponent,
    CommentComponent,
    NewCommentComponent,
    ContentComponent
  ],
  providers:[ PostService, CommentService ],
  exports: [PostComponent, VgCoreModule, VgControlsModule]
})
export class PostModule { }

TagInputModule.withDefaults({
  tagInput: {
    placeholder: '',
    // add here other default values for tag-input,
    secondaryPlaceholder: "e.g. funny, wait-for-it",
    modelAsStrings: true
  },
})
