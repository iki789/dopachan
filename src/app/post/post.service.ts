import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, retryWhen, delay } from 'rxjs/operators';

import { AuthService } from '../shared/auth.service'
import { Config } from "../config";
import { DpPostInterface } from './post.interface';
 
@Injectable()
export class PostService {

  constructor(private _http: HttpClient, private _auth: AuthService) { }

  getFeeds(section:string = "",queryParams: queryParamsInterface = {sort: 1, limit: 10, offset: 0}){
    let path = Config.host +  '/posts';
    let headers = {};
    let token =""
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    if(token){
      headers = new HttpHeaders({"Authorization": "Bearer " + token});
    }
    if(section){
      path = path +'/section/' + section;
    }
    return this._http.get( path + '?sort=' + queryParams.sort + '&limit=' + queryParams.limit + '&skip=' + queryParams.offset,{
      headers: headers
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))})
    );
  }

  getPost(id){
    let headers = {};
    let token =""
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    if(token){
      headers = new HttpHeaders({"Authorization": "Bearer " + token});
    }

    return this._http.get( Config.host + '/post/' + id ,{
      headers: headers
    });

  }

  getUserRecent(id, skip:number = 0){
    return this._http.get(Config.host + '/users/recent/' + id+'?skip='+skip,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (this._auth.getJwtToken() ? this._auth.getJwtToken() : '')
      })
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(2000))})
    );
  }

  getUserCollection(id, skip:number = 0){
    return this._http.get(Config.host + '/users/collection/' + id+'?skip='+skip,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (this._auth.getJwtToken() ? this._auth.getJwtToken() : '')
      })
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(2000))})
    );
  }

  getUserPosts(id, skip:number = 0){
    return this._http.get(Config.host + '/users/posts/' + id+'?skip='+skip,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (this._auth.getJwtToken() ? this._auth.getJwtToken() : '')
      })
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))})
    );
  }

  react(postId: string, reactId: string, remove: boolean=false){

    return this._http.post(Config.host + '/posts/' + postId + '/react',{id: reactId, to_remove: remove},{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    }).pipe(
      retry(3)
    );
  }

  getReportedPosts(skip=0){
    return this._http.get(Config.host + '/reports/post?skip='+skip, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))})
    );
  }

  getUnderReviewPosts(skip=0){
    return this._http.get(Config.host + '/reviews/post?skip='+skip, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    }).pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))})
    );
  }

  allow(post: DpPostInterface, savePost: boolean = false){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this._auth.getJwtToken()
    });
    let post_details: any = {id: post.id};
    let path = Config.host + '/reviews/post/allow';
    if(savePost == true){ 
      path = path + "?save=true"; 
      post_details = {id: post.id, caption: post.caption, tags: post.tags, nsfw: post.nsfw };;
    }
    return this._http.post(path, post_details,{
      headers: headers
    }).pipe(retryWhen(error=>{ return error.pipe(delay(2000))}))
  }

  

  getFeatured(){
    return this._http.get(Config.host + '/featured/posts').pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))})
    );
  }

  markAsFeatured(id){
    return this._http.post(Config.host + '/featured/posts/'+id,{},{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    }).pipe(
      retry(3)
    );
  }

  removeFromFeatured(id){
    return this._http.delete(Config.host + '/featured/posts/'+id,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      })
    }).pipe(
      retry(3)
    );
  }

  remove(id, poster_id: string, reason: number, ban?: boolean){
    let path = Config.host + '/post/'+id;
    let params:any = {};
    
    if(ban){ params.ban = ban; }
    params.poster_id = poster_id;
    params.reason = reason;
    
    return this._http.delete(path, {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this._auth.getJwtToken()
      }),
      params: params
    }).pipe(
      retry(3)
    );
  }
}

interface queryParamsInterface {
  'sort': number,
  'limit': number,
  'offset': number
}