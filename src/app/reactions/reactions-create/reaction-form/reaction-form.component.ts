import { Component, EventEmitter, Input, Output, OnInit, ViewContainerRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpEventType } from '@angular/common/http';

import { ReactionService } from '../../reactions.service';

@Component({
  selector: 'app-reaction-form',
  templateUrl: './reaction-form.component.html',
  styleUrls: ['./reaction-form.component.scss']
})

export class ReactionFormComponent implements OnInit {
  @Input() file : File;
  @Output() onRemove = new EventEmitter();
  imageBase64: string;
  size: string;
  name: string;
  uploadProgress:number = 0;
  uploaded: boolean = false ;
  uploadError: boolean = false;

  constructor(private sanitize: DomSanitizer, private vcr: ViewContainerRef, private reactionService: ReactionService) { }

  ngOnInit() {
    this.imageBase64 = URL.createObjectURL(this.file);
    this.size = this.formatBytes(this.file.size);
    this.name = this.file.name.substr(0, this.file.name.lastIndexOf(".")); 
  }

  sanitizeBlobURL(url){
    return this.sanitize.bypassSecurityTrustUrl(url);
  }

  formatBytes(bytes) {
    if(bytes < 1024) return bytes + " Bytes";
    else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " Kb";
    else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " Mb";
    else return(bytes / 1073741824).toFixed(3) + " Gb";
  } 

  remove(){
    this.vcr.element.nativeElement.parentElement.removeChild(this.vcr.element.nativeElement);
    return this.onRemove.emit(this.file);
  }

  create(){
    if(this.name.length < 3)return false;
    if(this.uploaded)return false;
    return this.reactionService.create(this.file, this.name).subscribe(event=>{
      if(event.type == HttpEventType.UploadProgress){
        let percent = Math.round(event.loaded/event.total * 100);
        this.uploadProgress = percent;
      }
      if(event.type == HttpEventType.Response){
        if(event.body){
          if((event.body as any).error){
            this.uploaded = false;
            this.uploadError = true;
          }else{
            this.uploaded = true;
            this.uploadError = false;
          }
        }
      }
    })
    
  }

}
