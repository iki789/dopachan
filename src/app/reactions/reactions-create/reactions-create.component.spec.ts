import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionsCreateComponent } from './reactions-create.component';

describe('ReactionsCreateComponent', () => {
  let component: ReactionsCreateComponent;
  let fixture: ComponentFixture<ReactionsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
