import { Component, ElementRef, HostBinding, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';

import { ReactionFormComponent } from "./reaction-form/reaction-form.component"

@Component({
  selector: 'app-reactions-create',
  templateUrl: './reactions-create.component.html',
  styleUrls: ['./reactions-create.component.scss']
})
export class ReactionsCreateComponent implements OnInit {
  @ViewChild('fileInput') fileInput : ElementRef;
  @ViewChildren('reactComp') reactComp : QueryList<ReactionFormComponent>;
  @HostBinding('class') class = 'col-md-10 mt-2';
  constructor() { }

  uploading: boolean = false;
  maxSize: number = 24000 ;
  reactions: File[] = [];
  errors: any[] = [] ;

  ngOnInit() { }
  openFileInput(){
    this.fileInput.nativeElement.click();
  }

  onFileInputChange(){
    let files = this.fileInput.nativeElement.files;
    this.errors = [];
    for(let i=0; i < files.length; i++){
      if(this.validFile(files[i])){
        this.reactions.push(files[i]);
      }
    }
  }

  createAll(){
    this.uploading = true;
    this.reactComp.forEach(r=>{
      r.create();
    })
  }

  clear(){
    this.uploading = false;
    this.reactions = [];
  }

  remove(toRemoveFile){
    // Creating object since File objects can not stringifiy and be compared
    this.reactions = this.reactions.filter(file=>{
      let fileObj = {
        'name': file.name,
        'lastModified': file.lastModified,
        'size': file.size,
        'type': file.type
      };

      let toRemoveFileObj = {
        'name': toRemoveFile.name,
        'lastModified': toRemoveFile.lastModified,
        'size': toRemoveFile.size,
        'type': toRemoveFile.type
      };
      
      return JSON.stringify(fileObj) != JSON.stringify(toRemoveFileObj) ? true : false; 
    })
  }

  validFile(file: File){
    if(file.size > this.maxSize){
      this.errors.push({
        file: file,
        error: 'size'
      });
      return false;
    }
    let fileType = file.type.split('/').pop();
    if(fileType == 'png' || fileType == 'jpeg' || fileType == 'jpg'){
      
    }else{
      this.errors.push({
        file: file,
        error: 'type'
      });
      return false;
    }
    return true;
  }

}
