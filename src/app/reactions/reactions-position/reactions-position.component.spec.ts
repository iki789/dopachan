import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionPositionComponent } from './reaction-position.component';

describe('ReactionPositionComponent', () => {
  let component: ReactionPositionComponent;
  let fixture: ComponentFixture<ReactionPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
