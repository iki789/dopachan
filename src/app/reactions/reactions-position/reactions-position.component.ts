import { Component, HostBinding, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Reaction } from '../reactions';
import { ReactionService } from '../reactions.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reaction-position',
  templateUrl: './reactions-position.component.html',
  styleUrls: ['./reactions-position.component.scss']
})
export class ReactionsPositionComponent implements OnInit {

  @HostBinding('class') class = 'col-md-10 mt-2';
  @ViewChildren('wrapper') wrapper : QueryList<ElementRef>;
  reactions: Reaction[];

  constructor(private reactionService: ReactionService, private _toast: ToastrService) { }

  ngOnInit() {
    this.reactionService.getAll().subscribe((res: Reaction[])=>{
      this.reactions = res;
      this.sortByPosition();  
    })
  }

  moveDown($event){
    let element = $event.target.parentElement.parentElement.parentElement;
    if(element.nextElementSibling){
      // Update Element position
      element.dataset.position =  parseInt(element.dataset.position) +1;
      element.nextElementSibling.dataset.position = parseInt(element.nextElementSibling.dataset.position) -1; 
      // Move element
      element.parentNode.insertBefore(element.nextElementSibling, element);
    }
  }
  
  moveUp($event){
    let element = $event.target.parentElement.parentElement.parentElement;
    if(element.previousElementSibling){
      // Update position
      element.dataset.position =  parseInt(element.dataset.position) -1;
      element.previousElementSibling.dataset.position = parseInt(element.previousElementSibling.dataset.position) +1;
      // Move element
      element.parentNode.insertBefore(element, element.previousElementSibling);
    }
    console.log(element);
  }

  moveToTop($event){
    let element = $event.target.parentElement.parentElement.parentElement;
    if(parseInt(element.dataset.position) !== 0 ){
      // Update position
      element.dataset.position =  0;
      // Move element
      element.parentNode.insertBefore(element, element.parentNode.firstElementChild);
      element.parentNode.childNodes.forEach((el, i)=>{
        if(i !== 0){
          el.dataset.position = i;
        }
      })
    }
  }

  moveToBottom($event){
    let element = $event.target.parentElement.parentElement.parentElement;
    if(parseInt(element.dataset.position) !==  element.parentNode.childNodes.length){
      // Update position
      element.dataset.position =  element.parentNode.childNodes.length;
      // Move element
      element.parentNode.insertBefore(element, element.parentNode.nextSibling);
      element.parentNode.childNodes.forEach((el, i)=>{
        if(i !== 0){
          el.dataset.position = i;
        }
      })
    }
  }

  sortByPosition(){
    this.reactions.sort((a:any,b:any)=>{
      return a.position - b.position;
    });
  }
  
  update(){
    this.reactions.forEach(r=>{
      this.wrapper.forEach(el=>{
        if(r.id == el.nativeElement.dataset.id){
          r.position = parseInt(el.nativeElement.dataset.position)
        }
      })
    })
    this.reactionService.updatePosition(this.reactions).subscribe((res)=>{
      this._toast.success("Updated positions successfully!");
    },err=>{
      this._toast.error("Something went wrong!");
    })
  }
}
