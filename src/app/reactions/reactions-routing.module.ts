import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReactionsComponent } from './reactions.component';
import { ReactionsCreateComponent } from './reactions-create/reactions-create.component';
import { ReactionsPositionComponent } from './reactions-position/reactions-position.component';

const routes: Routes = [
  { path: '', component: ReactionsComponent},
  { path: 'create', pathMatch: 'full', component: ReactionsCreateComponent},
  { path: 'positions', pathMatch: 'full', component: ReactionsPositionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactionsRoutingModule { }
