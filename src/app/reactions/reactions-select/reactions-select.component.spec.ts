import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionsSelectComponent } from './reactions-select.component';

describe('ReactionsSelectComponent', () => {
  let component: ReactionsSelectComponent;
  let fixture: ComponentFixture<ReactionsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionsSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
