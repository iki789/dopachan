import { Location } from '@angular/common'
import { Component, EventEmitter, Inject, Input, OnInit, Output, Renderer2, ViewContainerRef } from '@angular/core';
import { animate, state, style, trigger, transition } from '@angular/animations'

import { SubscriptionLike } from 'rxjs';

import { ReactionService } from '../reactions.service';
import { Reaction } from '../reactions';
import { UserMinimalInterface } from 'app/user/user.interface';
import { AuthService } from 'app/shared/auth.service';

@Component({
  selector: 'app-reactions-select',
  templateUrl: './reactions-select.component.html',
  styleUrls: ['./reactions-select.component.scss'],
  animations:[
    trigger('slideUpAnimation',[
      state('open', style({
        bottom: '0%'
      })),
      state('closed', style({
        bottom: '-80%'
      })),
      transition('open=>closed', animate('0.3s ease-out')),
      transition('closed=>open', animate('0.25s ease-in'))
    ])
  ]
})
export class ReactionsSelectComponent implements OnInit {
  @Input() reactions: Reaction[];
  reactionsClone: Reaction[];
  user: UserMinimalInterface;
  private _location: SubscriptionLike;
  // Used for the position when selecting from the create modal
  @Input() index;
  @Input() toRemoveReactions: Reaction[];
  @Output() onSelect = new EventEmitter();
  toggle = false;

  constructor(private _auth: AuthService, private _reaction: ReactionService, private location: Location, private vcr: ViewContainerRef, private renderer: Renderer2, @Inject('Window') window: Window) {
    this.renderer.addClass(document.body, 'no-scroll');
    // Add Location hash to history for back button compatibility
    window.location.hash = 'react';
  }

  ngOnInit() {
    if(!this.reactions){
      this._reaction.getAll().subscribe((r: Reaction[])=>{
        this.reactions = r;
        this.reactionsClone = r;
        this.reactions = this.reactions = this.filter('', this.toRemoveReactions);
      })
    }else{
      this.reactionsClone = this.reactions;
      this.reactions = this.reactions = this.filter('', this.toRemoveReactions);
    }
    setTimeout(()=>{
      this.toggle = true;
    },10)

    // Subscribe to Location for back history
    this._location =  this.location.subscribe(e=>{
      if(e.type == 'popstate'){
        this.close()
      }
    })

    if(this._auth.isLoggedIn()){
      this.user = this._auth.getUser();
    }
  }

  onKeyup($event){
    let keyword:string = ($event.target.value).toLowerCase();
    this.reactions = this.filter(keyword, this.toRemoveReactions);
  }

  select(reaction){
    this.toggle = false;
    setTimeout(()=>{
      this.onSelect.emit({reaction: reaction, index: this.index});  
    },300)
  }

  close(){
    this.toggle = false;
    setTimeout(()=>{
      this.onSelect.emit();  
    },300)
  }

  private destroyComponent(){
    this.vcr.element.nativeElement.parentElement.removeChild(this.vcr.element.nativeElement);
  }

  private filter(keyword="", filter: Reaction[]=[]){
    let filterIds = [];
    let reactions = this.reactionsClone; 
    
    // Remove used Reactions
    for(let reaction of filter){
      filterIds.push(reaction.id);
    }

    // Return them
    if(keyword.trim().length == 0){
      return reactions.filter(x=>{
        return filterIds.indexOf(x.id) == -1 ? true : false;
      })  
    }

    return reactions.filter(x=>{
      return x.name.indexOf(keyword) != -1 && filterIds.indexOf(x.id) == -1 ? true : false;
    })
  }
  ngOnDestroy(){
    let offsetY = window.pageYOffset
    this._location.unsubscribe();
    window.location.hash = '';
    window.scrollTo(0, offsetY);
    this.renderer.removeClass(document.body, 'no-scroll')
  }
}
