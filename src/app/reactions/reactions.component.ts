import { Component, HostBinding, OnInit } from '@angular/core';
import { Reaction } from '../reactions/reactions';
import { ReactionService } from './reactions.service';

@Component({
  selector: 'app-reactions',
  templateUrl: './reactions.component.html',
  styleUrls: ['./reactions.component.scss']
})
export class ReactionsComponent implements OnInit {
  reactions: Reaction[] ;
  @HostBinding('class') class = 'col-md-10 mt-2';

  constructor(private reactionService: ReactionService) { }

  ngOnInit() {
    this.reactionService.getAll().subscribe((res: Reaction[])=>{
      this.reactions = res;
    });
  }

  getReactions(keyword="", filter: Reaction[]=[]){
    let filterIds = [];
    let reactions = this.reactions; 
    for(let reaction of filter){
      filterIds.push(reaction.id);
    }
    if(keyword.trim().length == 0){
      return reactions.filter(x=>{
        return filterIds.indexOf(x.id) == -1 ? true : false;
      })  
    }
    
    return reactions.filter(x=>{
      return x.name.indexOf(keyword) != -1 && filterIds.indexOf(x.id) == -1 ? true : false;
    })
  }

  toggleDefault(reaction){
    if(reaction.default){
      this.reactionService.removeDefault(reaction.id).subscribe(()=>{
        reaction.default = false ;
      });
    }else{
      this.reactionService.setDefault(reaction.id).subscribe(()=>{
        reaction.default = true ;
      })
    }
  }
  
  remove(reaction){
    if(confirm(`Are you sure you want to delete reaction '${reaction.name}'?`)){
      this.reactionService.remove(reaction.id).subscribe(()=>{
        this.reactions = this.reactions.filter(r=>{
          return r.id != reaction.id ? true : false ;
        })
      })
    }
  }
}
