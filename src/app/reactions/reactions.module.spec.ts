import { ReactionsModule } from './reactions.module';

describe('ReactionsModule', () => {
  let reactionsModule: ReactionsModule;

  beforeEach(() => {
    reactionsModule = new ReactionsModule();
  });

  it('should create an instance', () => {
    expect(reactionsModule).toBeTruthy();
  });
});
