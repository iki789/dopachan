import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ReactionsComponent } from "./reactions.component";
import { ReactionFormComponent } from './reactions-create/reaction-form/reaction-form.component'
import { ReactionsCreateComponent } from "./reactions-create/reactions-create.component";
import { ReactionsPositionComponent } from "./reactions-position/reactions-position.component";
import { ReactionsRoutingModule } from './reactions-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactionsRoutingModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  declarations: [
    ReactionsComponent,
    ReactionsCreateComponent,
    ReactionsPositionComponent,
    ReactionFormComponent
  ],
  exports:[ReactionsComponent]
})
export class ReactionsModule { }
