import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest } from "@angular/common/http";

import { Config } from '../config';
import { delay, retryWhen, map } from "rxjs/operators";
import { Reaction } from "../reactions/reactions";

@Injectable({
  providedIn: 'root',
})
export class ReactionService{
  constructor(private _http: HttpClient){ }

  create(file: File, keywords: string){
    let formData = new FormData();
    formData.append('keywords', keywords);
    formData.append('file', file);
    
    let req = new HttpRequest('post', Config.host + '/reactions', formData,{
      reportProgress: true
    });

    return this._http.request(req);
  }

  getAll(){
    return this._http.get(Config.host + '/reactions').pipe(
      retryWhen(error=>{ return error.pipe(delay(5000))}),
      map((r:Reaction[])=>{
        return r.map(r=>{
          if(!r.count){ r.count = 0}
          return r;
        })
      })
    );
  }

  getDefault(){
    return this._http.get(Config.host + '/reactions/default');
  }

  remove(id){
    return this._http.delete(Config.host + '/reactions/'+id);
  }

  setDefault(id){
    return this._http.post(Config.host + '/reactions/'+id+'/default',{})
  }

  removeDefault(id){
    return this._http.post(Config.host + '/reactions/'+id+'/default/remove',{})
  }

  updatePosition(reactions: Reaction[]){
    return this._http.put(Config.host + '/reactions/position', {reactions: reactions});
  }

  filterDefault(reactions: Reaction[]){
    return reactions.filter(r=>{
      return r.default;
    });
  }
}