export interface Reaction {
  id: string,
  name: string,
  image: string,
  liked?: boolean
  count?: number ,
  position?: number,
  default?: boolean
}