import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ReportService } from '../report.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-report-create',
  templateUrl: './report-create.component.html',
  styleUrls: ['./report-create.component.scss'],
  providers: [ReportService]
})
export class ReportCreateComponent implements OnInit {

  @Input() content: {id: string, posterId: string, type: string};
  reasons: any[]=[
    {value: 1, reason: "Spam"},
    {value: 2, reason: "Pornography"},
    {value: 3, reason: "Hatred and bullying"},
    {value: 4, reason: "Self-Harm"},  
    {value: 5, reason: "Violent, gory and harmful content"},
    {value: 6, reason: "Illegal activities e.g. Drug Uses"},
    {value: 7, reason: "Deceptive content"},
    {value: 8, reason: "Copyright and trademark infringement"},
    {value: 9, reason: "I just don't like it"},
  ];

  form : FormGroup;

  constructor(private _modalRef: NgbActiveModal, private _fb: FormBuilder, private _toast: ToastrService, private _report: ReportService) { }

  ngOnInit() {
    this.form = this._fb.group({
      'reason': ['1', Validators.required]
    });
  }

  submit(){
    if(this.form.status != "VALID") return false;
    this._modalRef.close('remove');
    this._report.report({reason: this.form.controls.reason.value, posterId: this.content.posterId, type: this.content.type}, this.content.id)
      .subscribe(res=>{
        this._toast.success("Content reported");
      },err=>{
        this._toast.error("Something went wrong!");
      })
  }

  close(){
    this._modalRef.close();
  }

}
