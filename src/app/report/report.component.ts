import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VgAPI } from 'videogular2/core';

import { AuthService } from '../shared/auth.service';
import { PostService } from '../post/post.service';
import { ReportService } from './report.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  providers: [ ReportService ]
})

export class ReportComponent implements OnInit {

  report:any = [];
  noReports: boolean = false;
  private skipReports: number = 0;
  private vgApi;
  @HostBinding('class') class = 'col-md-10 mt-2';

  constructor(private _posts: PostService, private _report: ReportService, private _auth: AuthService, private _router: Router) {  }

  ngOnInit() {
    if(!this._auth.getJwtToken()){
      let admin = this._auth.getUser();
      if(admin.group < 60){
        this._router.navigate(['/'])
        return false;
      }
    }
    this.getReport();
  }

  getReport(){
    this._posts.getReportedPosts(this.skipReports).subscribe((report: any)=>{
      if(report.length < 1){this.noReports = true;}
      this.report = report;
    });
  }

  allow(){
    this._report.allow({reportId: this.report.id, contentId: this.report.content_id, type: 'post'}).subscribe(x=>{
      this.next();
    });
  }

  remove(banUserId = null){
    this._report.remove({reportId: this.report.id, contentId: this.report.content_id, type: 'post', banUserId: banUserId, reason:""})
      .subscribe(x=>{
        this.next();
      });
  }

  skip(){
    this.skipReports = this.skipReports +1;
    this.next();
  }

  next(){
    this.getReport();
  }

  onPlayerReady(api: VgAPI) {
    this.vgApi = api;
    if(this.report.post.isVid){
      let video = this.vgApi.getDefaultMedia().vgMedia;
      // Mute video
      video.volume = 0;
    }
  }

}