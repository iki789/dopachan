import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry } from "rxjs/operators";

import { AuthService } from '../shared/auth.service';
import { Config } from '../config';

@Injectable()
export class ReportService{

  constructor(private _auth: AuthService, private _http: HttpClient){ }

  report({reason: reason, posterId: posterId, type: type}, content_id){
    let token = ""
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    return this._http.post(Config.host + "/" + type + "/report/" + content_id,{
        reason: reason,
        poster_id: posterId,
        type: type
      },
      {'headers':  new HttpHeaders({"Authorization": "Bearer " + token}) }
    ).pipe(
      retry(3)
    );
  }

  allow({ reportId: reportId, contentId: contentId, type: type }){
    let token = ""
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    return this._http.post(Config.host + '/reports/' + type +'/allow', {
      report_id: reportId,
      content_id: contentId
    },{
      headers: new HttpHeaders({"Authorization": "Bearer " + token})
    }).pipe(
      retry(3)
    );
  }

  remove({ reportId: reportId, contentId: contentId, type: type, banUserId: banUser = null, reason: reason }){
    let token = ""
    if(this._auth.isLoggedIn()){
      token = this._auth.getJwtToken();
    }
    let url = Config.host + '/reports/' + type +'/remove';
    if(banUser){
      url = url + "?ban=" + banUser + "&reason=" + reason ;
    }
    return this._http.post(url, {
      report_id: reportId,
      content_id: contentId,
      type: type,
      banUser: banUser
    },{
      headers: new HttpHeaders({"Authorization": "Bearer " + token})
    }).pipe(
      retry(3)
    );
  }
}