import { Component, HostBinding, OnInit, ViewChild, ElementRef } from '@angular/core';

import { PostService } from '../post/post.service';
import { DpPostInterface } from '../post/post.interface';
import { LoaderService } from '../shared/loader.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  
  private skipReports: number = 0;
  post: DpPostInterface;
  noPosts: boolean = false;
  editMode: boolean = false;
  @ViewChild('removeOptions') removeOptions: ElementRef; 
  @ViewChild('banOptions') banOptions: ElementRef; 
  @HostBinding('class') class = 'col-md-10 mt-2';

  constructor(private _post: PostService, private _loader: LoaderService, private _toast: ToastrService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts(){
    this._post.getUnderReviewPosts(this.skipReports).subscribe((post: DpPostInterface[])=>{
      if(post.length < 1){ this.noPosts = true; }
      this.post = post[0];
    });
  }

  allow(save:boolean = false){
    this._loader.show()
    this._post.allow(this.post, save).subscribe(()=>{
      this._loader.hide();
      this.reset();
      this.getPosts();
    })
  }

  remove(ban: boolean = false){
    let reason:number;
    if(ban){
      reason = this.banOptions.nativeElement.value;
    }else{
      reason = this.removeOptions.nativeElement.value
    }
    this._loader.show();
    this._post.remove(this.post.id, this.post.user_id, reason, ban).subscribe((res: any)=>{
      if(!res.success){
        this._toast.error(res.error)
      }
      this._loader.hide();
      this.reset();
      this.getPosts();
    })
  }
  
  skip(){
    this.skipReports = this.skipReports +1;
    this.getPosts();
  }

  onUpdate(post){
    
  }

  banUser(){

  }

  reset(){
    this.editMode = false;
  }

}
