import { Component, Input, EventEmitter, Output, OnInit, HostBinding } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'search-component',
  template: `
    <form [formGroup]="searchForm" (ngSubmit)="search()">
      <div class="form-group d-flex">
        <input type="text" class="form-control" formControlName="search" placeholder="Search Meme" />
        <span class="search-icn d-none d-sm-block" (click)="search()"> &#8981; </span>
        </div>
    </form>  
      `,
  styleUrls:['search.component.scss'],
  host:{class: 'search'}
})

export class SearchComponent implements OnInit{
  @Input() input;
  @Output() onSubmit = new EventEmitter();
  @HostBinding('class') classes='m-0'
  searchForm: FormGroup;

  constructor(private _fb: FormBuilder, private router:Router){ }

  ngOnInit(){
    this.constructForm();
  }

  constructForm(){
    this.searchForm = this._fb.group({
      'search': ['']
    });
  }

  search(){
    this.onSubmit.emit(this.searchForm.controls.search.value);
    this.router.navigate(['p', this.searchForm.controls.search.value]);
  }
}
