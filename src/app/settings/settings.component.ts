import { Component, HostBinding, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';

import { AuthService } from "../shared/auth.service";
import { UserService } from "../user/user.service";
import { LoaderService } from "../shared/loader.service"
import { DpUserInterface } from "../user/user.interface";
// Config is used in HTML
import { Config } from '../config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [UserService]
})

export class SettingsComponent implements OnInit {

  @HostBinding("class") "class" = "col-12 col-md-6"; 
  @ViewChild('fileInput') fileInput : ElementRef;
  @ViewChild("avatarFrame") avatarFrame: ElementRef;
  section: string = "profile";
  randomImageName: number;
  dob;
  minAge = {day: 31, month: 12, year:(new Date()).getFullYear() - 13 };
  profileForm: FormGroup;
  accountForm: FormGroup;
  user: DpUserInterface;
  config = Config;
  private totalAvatars: number = 114;

  constructor(private _auth: AuthService,  private _loader: LoaderService, private _toastr: ToastrService, private _router: Router, private fb: FormBuilder, private userService: UserService) { 
    this.buildForms();
  }

  ngOnInit() { 
    if(!this._auth.getJwtToken()){
      this._router.navigate(['/'])
    }
    let token = this._auth.getJwtToken();

    this.userService.getDetails(token).subscribe((user:DpUserInterface)=>{
      this.user = user;
      if(!user.location){
        this.user.location = { country: '' };
      }
      if(!user.about){
        this.user.about = '';
      }
      if(!user.gender){
        this.user.gender = 0;
      }
      if(!user.email){
        this.user.email = '';
      }
      if(!user.password){
        this.user.password = '';
      }
      if(!user.new_password){
        this.user.new_password = '';
      }
      if(!user.dob){
        this.user.dob = {
          day: 0,
          month: 0,
          year: 0
        };
      }
      // Update form
      this.updateForm(this.user);
    });
  }
  
  buildForms(){
    this.profileForm = this.fb.group({
      "displayName": ['', [Validators.minLength(3), Validators.required]],
      "about": [''],
      "gender": [''],
      "country":[''],
      "dob": [''],
      "avatarFile":['']
    })

    this.accountForm = this.fb.group({
      "name": ['', [Validators.minLength(3), Validators.required]],
      "email": ['', [Validators.email, Validators.required]],
      "password": ['', [Validators.minLength(6)]],
      "newPassword": ['', Validators.minLength(6)]
    })
  }

  profileFormSubmit(){
    this._loader.show("Saving Profile...")
    if(this.fileInput.nativeElement.files[0]){
      this.userService.uploadAvatar(this.fileInput.nativeElement.files[0]).then(x=>{
        if((x as any).success != 1){
          this._loader.hide();
          this._toastr.error("Error", (x as any).error);
          return false;
        }else{
          this.user.avatar = (x as any).path;
          this.profileForm.get('avatarFile').setValue((x as any).path);
        }
        this.userService.update(this.prepareProfileForm()).subscribe(x=>{
          this._loader.hide();
          if((x as any).success == 1){
            this._auth.setJwt((x as any).token);
            this._toastr.success("Settings saved!");
            setTimeout(()=>{
              window.location.reload(true);
            },600)
          }else{
            this._toastr.error("Error", (x as any).error);
          }
        },err=>{
          this._toastr.error("Something went wrong!","Error");
        });
      },err=>{
        this._toastr.error("Something went wrong!","Error");
      })
    }else{
      this.userService.update(this.prepareProfileForm()).subscribe(x=>{
        this._loader.hide("Saved!");
        if((x as any).success == 1){
          this._auth.setJwt((x as any).token);
          this._toastr.success("Settings saved!");
          setTimeout(()=>{
            window.location.reload(true);
          },600)
        }else{
          this._toastr.error("Error", (x as any).error);
        }
      },err=>{
        this._toastr.error("Something went wrong!","Error");
      });
    }
  }
  
  private prepareProfileForm(){
    const userForm: DpUserInterface = {
      id: this.user.id,
      name: this.user.name,
      avatar: this.profileForm.get('avatarFile').value,
      account_type: this.user.account_type,
      display_name: this.profileForm.get('displayName').value,
      about: this.profileForm.get('about').value,
      gender: this.profileForm.get('gender').value,
      dob: this.profileForm.get('dob').value,
      location:{
        country: this.profileForm.get('country').value
      }
    };
    return userForm;
  }

  private updateForm(user: DpUserInterface){
    // Used for getting async data
    this.profileForm.setValue({
      avatarFile: user.avatar,
      displayName: user.display_name ,
      about: user.about,
      gender: user.gender,
      dob: user.dob,
      country: user.location.country
    });
    
    this.accountForm.setValue({
      name: user.name,
      email: user.email,
      password: user.password,
      newPassword: user.new_password
    });
  }

  accountFormSubmit(){
    this._loader.show("Saving...");
    let user = this.prepareAccountForm();
    this.userService.update(user).subscribe(x=>{
      this._loader.hide("Saved!");
      if((x as any).success == 1){
        this._auth.setJwt((x as any).token);
        this._toastr.success("Settings saved!");
        setTimeout(()=>{
          window.location.reload(true);
        },600);
      }else{
        this._loader.hide();
        this._toastr.error("Error", (x as any).error);
      }
    }, err=>{
      this._toastr.error("Something went wrong!","Error");
    });
  }

  private prepareAccountForm(){
    const userForm: DpUserInterface = {
      id: this.user.id,
      name: this.user.name,
      email: this.accountForm.get('email').value,
      avatar: this.user.avatar,
      account_type: this.user.account_type,
      display_name: this.user.display_name,
      about: this.user.about,
      password: this.accountForm.get('password').value,
      new_password: this.accountForm.get('newPassword').value
    };
    return userForm;
  }

  activeSection(section){
    this.section = section;
  }

  onchange(){
    let file = this.fileInput.nativeElement.files[0]; 
    if(this.validateFile(file)){
      let image = URL.createObjectURL(file);
      this.avatarFrame.nativeElement.src =  image;
    } 
  }
  openUploadFile(){
    (this.fileInput.nativeElement as HTMLElement).click();
  }

  private validateFile(file: File){
    let acceptedExt = ["png", "jpeg", "jpg"];
    let ext = file.name.split(".").pop();
    let size = file.size;
    let maxSize = 2 * 1024 * 1024;
    if(acceptedExt.indexOf(ext) > -1 || size < maxSize ){
      return true;
    }else{
      return false;
    }
  }

  getRandomImage(){ 
    let imageNumber = this.randomImageName = Math.floor(Math.random()*this.totalAvatars) + 1;
    this.user.avatar = "./assets/avatars/" + imageNumber + ".jpg";
    this.profileForm.get('avatarFile').setValue(this.user.avatar);
  }

}
