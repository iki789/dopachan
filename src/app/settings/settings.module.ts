import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedComponentModule } from '../shared/shared-components.module'
import { SettingsComponent } from './settings.component'
import { SettingsRoutingModule } from './settings-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    SettingsRoutingModule,
    SharedComponentModule
  ],
  declarations: [SettingsComponent],
  exports:[SettingsComponent]
})
export class SettingsModule { }
