import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { Config } from '../config';
import { UserMinimalInterface } from '../user/user.interface';

declare var gapi: any;
declare var FB: any;

@Injectable()
export class AuthService {

  private loggedIn;

  constructor() { 
    this.loggedIn = this.checkLoggedIn();
    if(this.loggedIn){
      // Init gAPI for sign out button
      this.initGAPI();
    }
  }

  login(user){
    user = {
      token: user.token
    }
    localStorage.setItem("auth", JSON.stringify(user));
    return true;
  }

  initGAPI(){
    // Google API
    // gapi.load('auth2', ()=>{
    //   console.log("Loaded")
    //   // Retrieve the singleton for the GoogleAuth library and set up the client.
    //    gapi.auth2.init({
    //     client_id: '512574912221-qvku9v3l0d7mg2gt48qkg7pokhmia7gl.apps.googleusercontent.com',
    //     cookiepolicy: 'single_host_origin',
    //     // Request scopes in addition to 'profile' and 'email'
    //     scope: 'profile'
    //   });
    // });
    // console.log(gapi)
  }

  setJwt(token){
    localStorage.setItem("auth", JSON.stringify({token: token}));
    return true;
  }

  private checkLoggedIn(){
    if(localStorage.getItem('auth') === null ){
      this.loggedIn = false;
      return false;
    }else{
      this.loggedIn = true;
      return true;
    }
  }

  isLoggedIn(){
    return this.checkLoggedIn() ;
  }

  logout(){
    let user = this.getUser(); 
    if(user){
      if((user as any).account_type == Config.ACCOUNT_TYPE_FB){
        this.logoutFB();
        localStorage.removeItem('auth');
        return true;
      }
      if((user as any).account_type == Config.ACCOUNT_TYPE_GOOG){
        // this.logoutGoogle().then(()=>{
        //   localStorage.removeItem('auth');
        //   return true;
        // })
        localStorage.removeItem('auth');
        return true;
      }
      if((user as any).account_type == Config.ACCOUNT_TYPE_LOCAL){
        localStorage.removeItem('auth');
        return true;
      }
    }else{
      localStorage.removeItem('auth');
      return true;
    }
  }

  getJwtToken():string{
    if(this.isLoggedIn()){
      let x = JSON.parse(localStorage.getItem('auth'));
      return x.token;
    }
  }

  private logoutFB(){
    FB.getLoginStatus(res=>{
      if(res.authResponse){
        return FB.logout();
      }
    });
  }

  // private logoutGoogle() :Promise<any> {
  //   var auth2 = gapi.auth2.getAuthInstance();
  //   return auth2.signOut();
  // }

  getToken(){
    let helper = new JwtHelperService();
    let token = this.getJwtToken();
    if(!token) return false;
    if(helper.isTokenExpired(token)){
      this.forceLogout();
      return false;
    }else{
      return helper.decodeToken(token);
    }
  }

  getUser(): any{
    let helper = new JwtHelperService();
    let token = this.getToken();
    if(token){
      if(this.checkLoggedIn){
        return JSON.parse(token.sub);
      }
    }
  }

  forceLogout(){
    localStorage.removeItem('auth');
    return true;
  }
}
