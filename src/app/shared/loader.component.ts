import { Component } from "@angular/core";

@Component({
  selector: "dp-loader",
  template:`
  <div class="loader-wrapper">
    <div class="loader">
      <img src="./assets/images/loader.svg" width="96px" />
      <div class="title" >{{label}}</div>
    </div>
  </div>
  `,
  styles:[`
    :host{
      width: 100vw;
      height: 100vh;
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1090;
    }
    .loader-wrapper{
      width: 100vw;
      height: 100vh;
      position: absolute;
      display: flex;
      justify-content: center;
      align-items: center;
      z-index: 1090;
      // Compensate for bootstrap container
      margin: auto -8px;
    }
    .loader-wrapper::before{
      content: "";
      width: 100vw;
      height: 100vh;
      position: absolute;
      background-color: #F5D657;
      z-index: -1;
      opacity: 0.6;
    }
    .loader{
      text-align: center; 
    }
    .title{
      font-size: 24px;
      color: #fff;
      text-align: center;
      margin-top: 8px;
    }
  `]
})

export class LoaderComponent{
  label: string;

}
