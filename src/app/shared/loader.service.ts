import { ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Injectable, Injector } from "@angular/core";

import { LoaderComponent } from "./loader.component";

@Injectable()
export class LoaderService{
  private visible: boolean;
  private label: string;
  private _loaderRef: any;

  constructor(private componentFactory: ComponentFactoryResolver, private appRef: ApplicationRef, private injector: Injector){ }

  show(label?: string){
    this.label = label;
    return new Promise((resolve, reject)=>{
      resolve(this.create());
    })
  }

  hide(label?: string){
    if(!this._loaderRef) return false;
    this.label = label;
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        resolve(this.destroy());
      }, 1000)
    });
  }

  private destroy(){
    // this.appRef.detachView(componentRef.hostView);
    return this._loaderRef.destroy();
  }

  private create(){
     // 1. Create a component reference from the component 
     const componentRef = this.componentFactory
      .resolveComponentFactory(LoaderComponent)
      .create(this.injector);
      this._loaderRef = componentRef;
      
      // Set component label to label
      componentRef.instance.label = this.label;
    // 2. Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(componentRef.hostView);
    
    // 3. Get DOM element from component
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;
    
    // 4. Append DOM element to the body
    return document.body.appendChild(domElem);
  }
}