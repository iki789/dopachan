import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

// Inviewport
import { InViewportModule } from 'ng-in-viewport';

// Components
import { ReactionsSelectComponent } from '../reactions/reactions-select/reactions-select.component' 


@NgModule({
  imports: [ CommonModule, RouterModule, NgbModule.forRoot(), InViewportModule ],
  declarations: [ ReactionsSelectComponent ],
  exports:[ ReactionsSelectComponent, NgbModule, InViewportModule ]
})
export class SharedComponentModule { }
