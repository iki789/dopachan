import { Location } from '@angular/common'
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {  NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { SubscriptionLike } from 'rxjs';

import { AuthService } from './../shared/auth.service';
import { UserService } from './../user/user.service';
import { LoaderService } from './../shared/loader.service';
declare var gapi: any;
declare var FB: any;

@Component({
  selector: 'app-sign-form',
  templateUrl: './sign-form.component.html',
  styleUrls: ['./sign-form.component.scss'],
  providers: [AuthService, UserService]
})

export class SignFormComponent implements OnInit {

  activeTab;
  signInForm: FormGroup;
  registerForm: FormGroup;

  googInit: boolean = true;
  fbInit: boolean = true;

  SignInglobalError: string;
  SignUpglobalError: string;
  _location: SubscriptionLike;
  @ViewChild('googleBtn') googleBtn: ElementRef;
  private googleAuth: any;

  constructor(
      private location: Location,
      private activeModal: NgbActiveModal, 
      private _fb: FormBuilder,  
      private _auth: AuthService, 
      private _user: UserService,
      private _loader: LoaderService,
      @Inject('Window') window: Window
    ) {
    this.activeTab = "login";
  }
  
  ngOnInit() { 
    if(typeof gapi !== 'undefined'){
      this.initGAPI();
    }else{
      this.googInit = false;
    }
    if(typeof FB == 'undefined'){
      this.fbInit = false;
    }
    this._location = this.location.subscribe(e=>{
      if(e.type == 'popstate' && e.state){
        // user navigated back
        this.activeModal.close();
      }
    })
    window.location.hash = 'sign';
    this.createforms();
  }

  initGAPI(){
    // Google API
    gapi.load('auth2', ()=>{
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      this.googleAuth = gapi.auth2.getAuthInstance();
      this.googleAuth.then(()=>{
        // Register custom button
        this.loginG(this.googleBtn.nativeElement);
      });
    });
  }

  loginG(navtiveElement: ElementRef){
    this.googleAuth.attachClickHandler(navtiveElement, {},
      (googleUser)=>{
        this._loader.show("Logging in...");
        let token = googleUser.getAuthResponse().id_token
        
        this._user.loginGoogleUser(token).subscribe(res=>{
          if((res as any).success == true ){
            this._auth.login({token: (res as any).token});
            this.activeModal.close();
            window.location.reload(true);
          }
        })
      }, function(error) {
      // alert(JSON.stringify(error, undefined, 2));
    });
  }

  createforms() {
    this.signInForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.registerForm = this._fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    }); 
  }

  submitSignInForm() {
    if(this.signInForm.status == "VALID"){
      this._loader.show("Logging in...").then(()=>{
        this._user.login(this.signInForm.controls.email.value, this.signInForm.controls.password.value ).subscribe(res=>{
          if((res as any).success){
            this._auth.login({token:(res as any).token ,display_name: (res as any).display_name});
            window.location.reload();
          }else{
            this.SignInglobalError = "Email password mismatch";
            return false;
          }
        })
      })
      .then((res:any)=>{
        if(!res){ this._loader.hide("") }
      })
    } 
  }

  submitRegisterForm() {
    if(this.registerForm.status == "VALID"){
      this._loader.show("Working").then(()=>{
        this._user.create(this.registerForm.value)
        .subscribe(res=>{ 
          if((res as any).success){
            this._auth.login({token: (res as any).token});
            window.location.reload(true);
          }else{
            this.SignUpglobalError = (res as any).error;
          }
          this._loader.hide(); 
        });
      })
    }
  }

  loginFB() {
    let scope = "public_profile,email";
    let fields = "id,first_name,last_name,email,picture";

    FB.getLoginStatus(res=>{
      if(res.authResponse){
        FB.logout();
        this._auth.logout();
      }
      FB.login(res=>{
        if(res){
          this._loader.show("Logging in...");
          this._user.loginFbUser(res.authResponse.accessToken).subscribe(res=>{
            // If success is true, auth login
            if((res as any).success == true ){
              this._auth.login({token: (res as any).token});
              this.activeModal.close();
              window.location.reload(true);
            }else{
              this._loader.hide();
            }
          })
        }else{
          this._loader.hide();
        } 
      }, {scope: scope});
      
    });

  }

  toggleLoginTab() { this.activeTab = 'login'; }

  toggleRegisterTab() { this.activeTab = 'register'; }

  closeModal(){
    this.activeModal.close();
  }

  private matchOtherValidator(otherControlName: string) {

    let thisControl: FormControl;
    let otherControl: FormControl;
  
    return function matchOtherValidate (control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }
      if (!otherControl) {
        return null;
      }
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }
      return null;
    }
  }
  ngOnDestroy(){
    let offsetY = window.pageYOffset
    this._location.unsubscribe();
    window.location.hash = '';
    window.scrollTo(0, offsetY);
  }
}