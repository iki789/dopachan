import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate } from "@angular/router";

import { AuthService } from '../shared/auth.service';

@Injectable()
export class AdminGaurd implements CanActivate {
  
  loggedIn: boolean;

  constructor(private _router: Router, private _auth: AuthService){ 
    this.loggedIn = this._auth.isLoggedIn();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    if(!this.loggedIn){
      // this._router.navigate(['/']);
      return false;
    }
    let user = this._auth.getUser();
    if(user.group < 60){
      console.log('admin')
      // this._router.navigate(['/']);
      return false;
    }
    return true;
  }
}