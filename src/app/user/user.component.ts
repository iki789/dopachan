import { Component, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute } from "@angular/router"

import { AuthService } from '../shared/auth.service';
import { PostService } from '../post/post.service'
import { UserService } from './user.service';
import { UserMinimalInterface } from './user.interface';
import { DpPostInterface }  from '../post/post.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [AuthService, PostService, UserService]
})
export class UserComponent implements OnInit {

  @HostBinding("class") "class" = "col-12 col-md-6"; 
  myself: UserMinimalInterface;
  user: UserMinimalInterface;
  active = "recent";
  end: boolean = false;
  recent = [];
  posts = [];
  collection = [];
  skip: number = 0;
  loading: boolean = false;
  constructor(private _user: UserService, private _post: PostService, private route: ActivatedRoute, private _auth: AuthService) { }

  ngOnInit() {
    if(this._auth.isLoggedIn){
      this.myself = this._auth.getUser();
    }
    this._user.find(this.route.snapshot.params.id).subscribe((user: UserMinimalInterface)=>{
      this.user = user ;
      if(this.active == 'recent'){
        this.getRecentPosts({visible: true});
      }
    });
  }  

  getRecentPosts($event){
    if(!$event.visible) return false;
    this.skip = this.active == 'recent' ? this.skip : 0;
    if(this.end) return false;
    this.loading = true;
    this._post.getUserRecent(this.user.id, this.skip).subscribe((recentPosts: DpPostInterface[])=>{
      this.end = recentPosts.length > 0 ? false : true ;  
      this.recent = this.recent.concat(recentPosts);
      this.skip = this.recent.length
      this.loading = false;
    })
  }

  getUserCollection($event){
    if(!$event.visible) return false;
    this.active = 'collection';
    if(this.end) return false;
    this.loading = true;
    this._post.getUserCollection(this.user.id, this.skip).subscribe((collectionPosts: DpPostInterface[])=>{
      this.end = collectionPosts.length > 0 ? false : true ;  
      this.collection = this.collection.concat(collectionPosts);
      this.skip = this.collection.length
      this.loading = false;
    })
  }

  getUserPosts($event){
    if(!$event.visible) return false;
    this.active = 'posts';
    if(this.end) return false;
    this.loading = true;
    this._post.getUserPosts(this.user.id, this.skip).subscribe((usersPosts: DpPostInterface[])=>{
      this.end = usersPosts.length > 0 ? false : true ;  
      this.posts = this.posts.concat(usersPosts);
      this.skip = this.posts.length
      this.loading = false;
    })
  }

  switch(section: string){
    // init
    this.active = section;
    this.skip = 0;
    this.end = false;
    this.posts = [];
    this.recent = [];
    this.collection = [];

    if(section == 'posts'){
      this.getUserPosts({visible: true});
    }
    if(section == 'recent'){
      this.getRecentPosts({visible: true});
    }
    if(section == 'collection'){
      this.getUserCollection({visible: true});
    }
  }
}
