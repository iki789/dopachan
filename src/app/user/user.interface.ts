export interface UserMinimalInterface{
  id: string,
  avatar: string,
  display_name: string,
  fb_id?: string,
  account_type?: number,
  group: number
}

export interface DpUserInterface{
  id: string,
  name: string,
  display_name: string,
  email?: string,
  about: string,
  avatar: string,
  account_type: number,
  password?: string,
  new_password?: string,
  location?: {
    country?: string,
    city?: string
  },
  gender?: number,
  dob?: {
    day: number,
    month: number,
    year: number
  },
  fb_id?: string
}
