import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { retry, retryWhen, delay } from 'rxjs/operators'

import { AuthService } from "../shared/auth.service"
import { DpUserInterface } from "./user.interface";
import { Config } from '../config';

@Injectable()
export class UserService {
  private token;
  constructor(private http: HttpClient, private _auth: AuthService) {
    if(this._auth.isLoggedIn()){
      this.token = this._auth.getJwtToken();
    }
   }

  create(user: DpUserInterface){
    return this.http.post(Config.host + "/users/register", {user, account_type: Config.ACCOUNT_TYPE_LOCAL}).pipe(retry(3))
  }

  loginFbUser(token: string){
    return this.http.post(Config.host + "/users/login/fb", {token: token, account_type: Config.ACCOUNT_TYPE_FB});
  }

  loginGoogleUser(token: string){
    return this.http.post(Config.host + "/users/login/google", {token: token});
  }

  update(user: DpUserInterface){
    return this.http.put(Config.host + "/users", user,{
      headers: new HttpHeaders({"Authorization": "Bearer " + this.token})
    }).pipe(retry(3));
  }

  find(id: string){
    return this.http.get(Config.host + "/users/" + id).pipe(retry(3));
  }

  findAll(offset: number = 0, limit: number = 20, params: HttpParams){
    return this.http.get(Config.host + '/users?skip='+offset+'&limit='+limit ,{
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token
      }),
      params: params
    }).pipe(retryWhen(error=>{ return error.pipe(delay(5000))}))
  }

  getDetails(token: string){
    return this.http.get(Config.host + "/users/current/", { 
      headers: new HttpHeaders({"Authorization": "Bearer " + this.token}) 
    }).pipe(retry(3));
  }

  uploadAvatar(file: File){
    return new Promise((resolve,reject)=>{
      let formData = new FormData();
      formData.append("file", file);
      this.http.post(Config.host + "/users/avatar", formData, {
        headers: new HttpHeaders({"Authorization": "Bearer " + this.token})
      }).subscribe(x=>{
        resolve(x);
      })
    });
  }

  addPostToCollection(postId){
    return this.http.post(Config.host + '/post/save/'+postId,{},{
      headers: new HttpHeaders({"Authorization": "Bearer " + this.token})
    })
  }

  removePostFromCollection(postId){
    return this.http.post(Config.host + '/post/saved/remove/'+postId,{},{
      headers: new HttpHeaders({"Authorization": "Bearer " + this.token})
    })
  }
  
  login(email, password){
    return this.http.post(Config.host + '/users/login',{email: email, password: password});
  }
  
}