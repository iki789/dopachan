import { Component, HostBinding, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { HttpParams } from '../../../node_modules/@angular/common/http';

import { UserService } from '../user/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers:[UserService]
})
export class UsersComponent implements OnInit {

  search: any;
  fullAdmins: any;
  seniorAdmins: any;
  admins: any;
  moderators: any;
  usersParam: any;
  users: any[];
  searchForm: FormGroup;
  @HostBinding('class') class = 'col-md-10 mt-2';

  constructor(private _fb: FormBuilder, private _user: UserService, public snapshot: ActivatedRoute, private router: Router) { }

  ngOnInit() { 
    this.snapshot.queryParamMap.subscribe((params)=>{
      this.search = params.get('q') ? params.get('q') : '';
      this.fullAdmins = params.get('full-admins') == 'true' ? params.get('full-admins') : '' ;
      this.seniorAdmins = params.get('senior-admins') == 'true' ? params.get('senior-admins') : '' ;
      this.admins = params.get('admins') == 'true' ? params.get('admins') : '' ;
      this.moderators = params.get('moderators') == 'true' ? params.get('moderators') : '' ;
      this.usersParam = params.get('users') == 'true' ? params.get('users') : '' ;
      
      this.initForms();
      
      if(params.get('submit')){
        this.formSubmit();
      }
    })
    
  }

  initForms(){
    this.searchForm = this._fb.group({
      'search': [this.search],
      'fullAdmins': [this.fullAdmins],
      'seniorAdmins': [this.seniorAdmins],
      'admins': [this.admins],
      'moderators': [this.moderators],
      'users': [this.usersParam],
    })
  }

  formSubmit(){
    let params: Params = {
      'q': this.searchForm.controls.search.value,
      'full-admins': this.fullAdmins,
      'senior-admins': this.seniorAdmins,
      'admins': this.admins,
      'moderators': this.moderators,
      'users': this.usersParam,
      'submit': true
    }
    this.getUsers();
    
    this.router.navigate(['users'],{
      queryParams: params
    });

  }

  getUsers(){
    let httpParams = new HttpParams()
      .set('q', this.searchForm.controls.search.value)
      .set('full-admins', this.fullAdmins)
      .set('senior-admins', this.seniorAdmins)
      .set('admins', this.admins)
      .set('moderators', this.moderators)
      .set('users', this.usersParam)

    this._user.findAll(0,10,httpParams).subscribe((users: any[])=>{
      this.users = users;
    });
  
  }

}
